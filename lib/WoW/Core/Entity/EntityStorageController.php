<?php

/**
 * @file
 * Contains EntityStorageController.
 */

namespace WoW\Core\Entity;

use EntityAPIController;

/**
 * Defines a base entity storage controller class.
 */
class EntityStorageController extends EntityAPIController {

  public function loadBy($region) {}

}
