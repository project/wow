<?php

/**
 * @file
 * Definition of RealmServiceController.
 */

namespace WoW\Character\Entity;

use WoW\Core\Entity\EntityServiceController;
use WoW\Core\Response;
use WoW\Core\ServiceInterface;

/**
 * Service controller class for realms.
 */
class CharacterServiceController extends EntityServiceController {

  /**
   * The list of fields supported by the character resource end point.
   *
   * @var array
   */
  public static $fields = array(
    'achievements',
    'appearance',
    'feed',
    'guild',
    'hunterPets',
    'items',
    'mounts',
    'pets',
    'petSlots',
    'professions',
    'progression',
    'pvp',
    'quest',
    'reputation',
    'stats',
    'talents',
    'titles'
  );

  /**
   * Realm APIs currently provide realm status information.
   *
   * @param Character $character
   *   The realm to fetch.
   *
   * @return Response
   *   A response object.
   */
  public function fetch(Character $character) {

    $fields = array();
    // Fields are user defined: builds the 'fields' parameter with only what is
    // needed by the entity.
    foreach (self::$fields as $key) {
      if (isset($character->{$key})) {
        $fields[] = $key;
      }
    }

    // Returns the execution of the request on this service.
    return $this->service($realm->region)
      ->newRequest("character/$character->realm/$character->name")
        ->setQuery('fields', $fields)
        ->setLocale($character->language)
        ->setIfModifiedSince($character->lastModified)
        ->execute();
  }

}
