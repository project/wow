<?php

/**
 * @file
 * Definition of Character.
 */

namespace WoW\Character\Entity;

use WoW\Core\Entity\Entity;
use WoW\Core\Entity\EntityServiceController;

use WoW\Core\Response;

/**
 * Defines the wow_realm entity class.
 */
class Realm extends Entity {

  /**
   * The character ID.
   *
   * @var integer
   */
  public $cid;

  /**
   * The character owner's user ID.
   *
   * @var integer
   */
  public $uid;

  /**
   * The character realm (slug).
   *
   * @var string
   */
  public $realm;

  /**
   * The character name.
   *
   * @var string
   */
  public $name;

  /**
   * The character level.
   *
   * @var integer
   */
  public $level;

  /**
   * Whether the character is active(1) or blocked(0).
   *
   * @var integer
   */
  public $status;

  /**
   * Whether the character is main(1) or alt(0).
   *
   * @var integer
   */
  public $isMain;

  /**
   * The character thumbnail.
   *
   * @var string
   */
  public $thumbnail;

  /**
   * The character race.
   *
   * @var integer
   */
  public $race;

  /**
   * The character achievement points.
   *
   * @var integer
   */
  public $achievementPoints;

  /**
   * The character gender.
   *
   * @var integer
   */
  public $gender;

  /**
   * The character class.
   *
   * @var integer
   */
  public $class;

  /**
   * Realm APIs currently provide realm status information.
   *
   * @return Response
   *   A response object.
   */
  public function fetch() {
    return wow_service_controller($this->entityType)->fetch($this);
  }

}
