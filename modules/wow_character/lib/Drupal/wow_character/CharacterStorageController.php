<?php

/**
 * @file
 * Definition of WoW\Character\CharacterStorageController.
 */

namespace Drupal\wow_character;

use Drupal\wow\Entity\RemoteStorageController;

/**
 * Controller class for characters.
 *
 * This extends the EntityStorageController class, adding required special
 * handling for character objects.
 */
class CharacterStorageController extends RemoteStorageController {

}
