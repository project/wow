<?php

/**
 * @file
 * Definition of Guild.
 */

namespace WoW\Guild\Entity;

use WoW\Core\Entity\Entity;
use WoW\Core\Entity\EntityServiceController;

use WoW\Core\Response;

/**
 * Defines the wow_realm entity class.
 */
class Guild extends Entity {

  /**
   * The guild ID.
   *
   * @var integer
   */
  public $gid;

  /**
   * The guild realm (slug).
   *
   * @var string
   */
  public $realm;

  /**
   * The guild name.
   *
   * @var string
   */
  public $name;

  /**
   * The guild level.
   *
   * @var integer
   */
  public $level;

  /**
   * Whether the guild is alliance(0) or horde(1).
   *
   * @var integer
   */
  public $side;

  /**
   * The guild achievement points.
   *
   * @var integer
   */
  public $achievementPoints;

  /**
   * Realm APIs currently provide realm status information.
   *
   * @return Response
   *   A response object.
   */
  public function fetch() {
    return wow_service_controller($this->entityType)->fetch($this);
  }

}
